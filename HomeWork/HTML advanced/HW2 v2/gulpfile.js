const gulp = require("gulp");
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const terser = require('gulp-terser');
const cleanCss = require('gulp-clean-css');
const imgmin = require('gulp-imagemin');
const del = require('del');
const browserSync = require('browser-sync');

const cssSors=[
    "./src/scss/**/*.scss"
];

const jsSors=[
    "./src/js/scrypt_src.js"
];

const css_func = function () {
    return gulp.src(cssSors)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.min.css'))
        .pipe(autoprefixer({cascade: false}))
        .pipe(cleanCss({level: 2}))
        .pipe(gulp.dest("./dist/css"))
        .pipe(browserSync.stream());
};

const js_func = function () {
    return gulp
        .src(jsSors)
        .pipe(concat('script.min.js'))
        .pipe(terser())
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream());
};

const img_func = function () {
    return gulp.src("./src/img/**/*.*")
        .pipe(imgmin())
        .pipe(gulp.dest('./dist/img'))
        .pipe(browserSync.stream())
};

function remove_func() {
    return del(['./dist/*']);
}

function watch(){
    browserSync.init({
        server: {
            baseDir: './'
        }
    });
    gulp.watch('./src/scss/**/*.scss', css_func);
    gulp.watch('./src/js/**/*.js', js_func);
    gulp.watch('./src/image/**/*.jpg', img_func);
    gulp.watch("./*.html", browserSync.reload);
}

gulp.task("moveCSS", css_func);
gulp.task("moveJS", js_func);
gulp.task("moveIMG", img_func);
gulp.task("rem", remove_func);
gulp.task('watch', watch);
gulp.task('build', gulp.series(remove_func, gulp.parallel(css_func, js_func, img_func)));
gulp.task('dev', gulp.series('build', watch));
