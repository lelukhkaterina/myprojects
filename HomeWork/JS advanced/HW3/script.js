// Задание
//
// Реализовать класс Employee, в котором будут следующие свойства -
// name (имя), age (возраст), salary (зарплата).
// Сделайте так, чтобы эти свойства заполнялись при создании объекта.
//     Сделайте геттеры и сеттеры для этих свойств.
//     Сделайте класс Programmer, который будет наследоваться от класса Employee,
//     у которого будет свойство lang (список языков)
// Для класса Programmer перезапишите геттер для свойства salary.
// Пусть он возвращает свойство salary умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

class Employee {
    constructor(name, age, salary) {
        this._name=name;
        this._age=age;
        this._salary=salary;
    }
    get name() {
        return this._name;
    }

    set name(value) {
       return this._name = value;
    }


    get age() {
        return this._age;
    }

    set age(value) {
        return this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        return this._salary = value;
    }

}

let worker1=new Employee('Katya', 25, 20000);
let worker2=new Employee('Vasya',30, 15000);
let worker3=new Employee('Lisa', 75, 5000);
let worker4=new Employee('Petya', 18, 500);

console.log(worker1, worker2, worker3, worker4);


class Programmer extends Employee{
    constructor(lang, ...otherParam) {
        super(...otherParam);
        this.lang=lang;
    }
    get salary(){
        return this._salary*3;
    }
    set salary(value) {
        return this._salary = value;
    }
}

prog1=new Programmer("C++", "Lesha", 23, 500000);
prog2=new Programmer("Assembler ", "Marina", 28, 200000);
prog3=new Programmer("Delphi", "Lesha", 23, 500000);

console.log(prog1, prog2, prog3);