window.onload = function () {

    let buttonIp = document.querySelector("#myIp");
    buttonIp.addEventListener('click', function (event) {

        const _API = "https://api.ipify.org/?format=json";
        const _APIIP = "http://ip-api.com";

        fetch(`${_API}`)
            .then(my_result => {
                if (my_result.status === 200) {
                    return my_result.json();
                } else {
                    throw new Error(my_result.status);
                }
            })
            .then(my_result => {
                console.log(my_result.ip);
                fetch(`${_APIIP}/json/${my_result.ip}`)
                    .then(data => {
                        if (data.status === 200) {
                            return data.json()
                        } else {
                            throw new Error(data.status)
                        }
                    })
                    .then(data => {
                        console.log(data);
                        showInfo(data);
                    })
                    .catch(e => console.error(e));

            })
            .catch(e => console.error(e));

        function showInfo(obj) {
            let startEl = document.querySelector("ul");
            if (startEl !== null) {
                startEl.remove();
            }
            let ulEl = document.createElement("ul");
            let myAddr = ["query", "city", "country", "timezone", "zip"];
            myAddr.forEach(function (item) {
                let liEl = document.createElement("li");
                liEl.innerHTML = `${item}: ${obj[item]}`;
                ulEl.append(liEl);
                console.log(`${item}: ${obj[item]}`);
            });
            buttonIp.after(ulEl);
        }
    })
};