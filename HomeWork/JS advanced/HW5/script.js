window.onload = function () {

    const _API = "https://api.ipify.org/?format=json";
    const _APIIP = "http://ip-api.com/";

    let buttonIp = document.querySelector("#myIp");
        buttonIp.addEventListener('click', findByIp);

    async function findByIp(){
        try {
            let response = await fetch(`${_API}`);
            let userIp;
            if (response.status === 200) {
                userIp = await response.json();
            } else {
                throw new Error(response.status);
            }
            console.log(userIp);
            try {
                let responseAPIIP = await fetch(`${_APIIP}/json/${userIp.ip}`);
                if (responseAPIIP.status === 200) {
                    userAddress = await responseAPIIP.json();
                } else {
                    throw new Error(responseAPIIP.status);
                }
                console.log(userAddress);
                showInfo(userAddress);
            } catch (e) {
                console.error(e);
            }

        }catch (e) {
            console.error(e);
        }
    }


    function showInfo(obj) {
        let startEl = document.querySelector("ul");
        if (startEl !== null) {
            startEl.remove();
        }
        let ulEl = document.createElement("ul");
        let myAddr = ["query", "city", "country", "timezone", "zip"];
        myAddr.forEach(function (item) {
            let liEl = document.createElement("li");
            liEl.innerHTML = `${item}: ${obj[item]}`;
            ulEl.append(liEl);
            console.log(`${item}: ${obj[item]}`);
        });
        buttonIp.after(ulEl);
    }

};