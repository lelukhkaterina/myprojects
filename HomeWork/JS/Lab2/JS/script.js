alert('Первый пример: Реализовать программу на Javascript, которая будет находить все числа кратные 5 (делятся на 5 без остатка) в заданном диапазоне.');

let numberStop = parseFloat(prompt('Enter Number Stop'));
while (!Number.isInteger(numberStop) || isNaN(numberStop)){
    numberStop = parseFloat(prompt('Enter Number Stop'));
}

if (numberStop>5) {
    for (let counter = 1; counter < numberStop; counter++) {
        if (counter % 5 === 0) {
            console.log(counter);
        }
    }
}else {
    console.log('Sorry, no numbers');
}
