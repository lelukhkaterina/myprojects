alert('Второй пример: Считать два числа, m и n. Вывести в консоль все простые числа');
let m=parseFloat(prompt('Enter min number (Число должно быть целым, больше 0)'));
let n=parseFloat(prompt('Enter max number (Число должно быть целым, больше 0)'));
    while ((m>=n) || (m<0) || (n<0) || !Number.isInteger(m) || isNaN(m)|| !Number.isInteger(n) || isNaN(n)){
        m=parseFloat(prompt('Enter min number (Число должно быть целым, больше 0)'));
        n=parseFloat(prompt('Enter max number (Число должно быть целым, больше 0)'));
    }

label:
    for (let i=m; i<=n; i++){   // ot m do n
        for (let j=2; j<i; j++){ //ot 2 do mn
            if(i%j===0){
                continue label;
            }
        }
        console.log(i);
    }