const newPassEl=document.querySelector(".newPass");
const newPassRepeatEl=document.querySelector(".newPassRepeat");
const formEl=document.querySelector("form");
const buttonEl=document.querySelector("button");
const ErrorText=document.createElement("span");
    ErrorText.innerHTML="Нужно ввести одинаковые значения";
    ErrorText.style.color="red";
    ErrorText.style.visibility="hidden";
    ErrorText.style.marginBottom="20px";
buttonEl.before(ErrorText);

formEl.addEventListener("click", (event)=>{
    let target=event.target;

    if(target.tagName==="I") { //Если нажали на глазок, сменить значек и изменить значение input type
        target.classList.toggle("fa-eye-slash");
        target.classList.toggle("fa-eye");
        let inputEl=target.previousElementSibling;
        switch (inputEl.getAttribute("type")) {
            case 'text':
                inputEl.setAttribute ("type","password");
                break;
            case 'password':
                inputEl.setAttribute ("type","text");
                break;
        }
    }

    if(target.tagName==="BUTTON"){ //Если нажали на кнопку проверить соответствие паролей и вывести сообщение
        console.log(event);
        event.preventDefault();
        if(newPassEl.value===newPassRepeatEl.value){
            alert("You are welcome");
        }
        else{
            ErrorText.style.visibility="visible";
        }
    }

    if(target.tagName==="INPUT"){ // при нажатии по инпуту, спрятать текст ошибки
        ErrorText.style.visibility="hidden";
    }
});