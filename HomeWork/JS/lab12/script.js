const imgEls=document.querySelectorAll("img");
const buttonStart=document.querySelector(".start");
const buttonStop=document.querySelector(".stop");
const timerEl=document.querySelector(".timer");
const timout=10000; //Как часто меняем картинки;
console.log(imgEls);

let i=1;
let oneClick=false;
let intervalTime;
let time=timout;

//Функция показа и скрытия картинки;
function showPic() {
    console.log(i);
    //Если не первая и не последняя, текущую показываем, предыдущую прячем;
    if (i>0 && i<imgEls.length){
        imgEls[i-1].classList.add("none");
        imgEls[i].classList.remove("none");
        i++;
    }
    //Если первая, показываем первую, прячем последнюю;
    else if (i===0){
        imgEls[imgEls.length-1].classList.add("none");
        imgEls[i].classList.remove("none");
        i++;
    }
    //Если последняя+1 переменной і даем значение первого элемента;
    if (i===imgEls.length){
        i=0;
    }
}

//Функция таймера, при истечении времени запускаем снова setInterval и показываем новую картинку;
function timeF() {
    let timeElShow=time/1000;
    timerEl.innerHTML=`${timeElShow.toFixed(3)}`;
    time=time-10;
    if (time===0){
        clearInterval(intervalTime);
        time=timout;
        intervalTime = setInterval(timeF, 10);
        showPic ();
    }
}

//Обработка клавиши Start, показываем таймер, запускаем функции листания картинок и отсчета таймера;
buttonStart.addEventListener("click", () => {
    if (!oneClick) {
        timerEl.classList.remove("none");
        intervalTime = setInterval(timeF, 10);
        oneClick=true; //Вешаем флаг на одноразовое нажатие кномки;
        // time=timout; // Если останавливали, начать отсчет времени с начала;
    }
});

//Обработка кнопки Stop, останавливает листание картинок и таймер;
buttonStop.addEventListener("click",()=>{
    clearInterval(intervalTime);
    oneClick=false;
});

