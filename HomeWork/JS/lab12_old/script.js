const imgEls=document.querySelectorAll("img");
const buttonStart=document.querySelector(".start");
const buttonStop=document.querySelector(".stop");
const timerEl=document.querySelector(".timer");
const timout=10000; //Как часто меняем картинки;
console.log(timerEl);

let i=1;
let interval;
let oneClick=false;
let intervalTime;
let time=timout;


//Функция таймера, при истечении времени запускаем снова setInterval;
function timeF() {
    let timeElShow=time/1000;
    timerEl.innerHTML=`${timeElShow.toFixed(3)}`;
    time=time-1;
    if (time===0){
        clearInterval(intervalTime);
        time=timout;
        intervalTime = setInterval(timeF, 1);
    }
}

//Функция показа и смены картинки;
function showPic() {
    console.log(i);
    //Если не первая и не последняя, текущую показываем, предыдущую прячем;
    if (i>0 && i<imgEls.length){
        imgEls[i-1].classList.add("none");
        imgEls[i].classList.remove("none");
        i++;
    }

    //Если первая, показываем первую, прячем последнюю;
    if (i===0){
        imgEls[imgEls.length-1].classList.add("none");
        imgEls[i].classList.remove("none");
        i++;
    }

    //Если последняя+1 переменной і даем значение первого элемента;
    if (i===imgEls.length){
        i=0;

    }
}

//Обработка клавиши Start, показываем таймер, запускаем функции листания картинок и отсчета таймера;
buttonStart.addEventListener("click", () => {
    if (!oneClick) {
        timerEl.classList.remove("none");
        interval = setInterval(showPic, timout);
        intervalTime = setInterval(timeF, 1);
        oneClick=true; //Вешаем флаг на одноразовое нажатие кномки;
        time=timout; // Если останавливали, начать отсчет времени с начала;
    }
});

//Обработка кнопки Stop, останавливает листание картинок и таймер;
buttonStop.addEventListener("click",()=>{
    clearInterval(interval);
    clearInterval(intervalTime);
    oneClick=false;
});

