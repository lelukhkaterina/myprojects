const buttonEl = document.querySelectorAll("button");
const buttonTem = document.querySelector("#newTem");
const bodyEl = document.querySelector('body');
const classicTem = {
    backgroundColor: 'darkseagreen',
    buttonColor: 'cadetblue',
};
const newTem = {
    backgroundColor: 'darkgrey',
    buttonColor: 'lightgrey',
};
let tem;

function addStyle(tem) {
    bodyEl.style.backgroundColor = tem.backgroundColor;
    buttonTem.style.backgroundColor = tem.buttonColor;
}

function temInspect() {
    if (!localStorage.getItem('flagTem')) {
        tem = classicTem;
    } else {
        tem = newTem;
    }
    addStyle(tem);
}

function changeTem() {
    if (!localStorage.getItem('flagTem')) {
        tem = newTem;
        localStorage.setItem('flagTem', 'true');
    }
    else{
        tem = classicTem;
        localStorage.removeItem('flagTem');
    }
    addStyle(tem);
}

temInspect();
buttonTem.addEventListener("click", changeTem);

document.addEventListener("keydown", function (event) {
    buttonEl.forEach((item) => {
        item.classList.remove("btnBlue");
        if (item.innerHTML.toLowerCase() === event.key.toLowerCase()) {
            item.classList.add("btnBlue");
        }
    });
});



