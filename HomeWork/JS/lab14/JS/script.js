const btnToTop = $('.scroll-top');
const checkScrollingViewPort = (event) => {
    const scrolling = window.scrollY;
    const windowHeight = window.innerHeight;
    if (scrolling > windowHeight) {
        btnToTop.fadeIn(100);
    } else {
        btnToTop.fadeOut(100);
    }
};
// checkScrollingViewPort();
$(window).scroll(checkScrollingViewPort);
btnToTop.on('click', () => {
    window.scrollTo({
        top: 0,
        behavior: 'smooth',
    });
});

//Повесила на родителя все пункты меню
$(".main_menu_block").on("click", function () {
    let targetMenu=event.target.dataset.menu;
    $('html, body').animate({
        scrollTop: $(`.${targetMenu}`).offset().top  // класс объекта к которому приезжаем
    }, 1000); // Скорость прокрутки
});

//Сворачивать/разворачивать блок header_2
$(".slideTog").on("click", function() {
    $(".header_2").slideToggle(1500);
});

