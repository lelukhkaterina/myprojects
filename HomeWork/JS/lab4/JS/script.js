console.log(`
Задание
Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript, без 
использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с 
фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль 
результат выполнения функции.

Необязательное задание продвинутой сложности:
Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName()
 и setLastName(), которые позволят изменить данные свойства.
 
 Сменить имя: user.setFirstName;
 Сменить фамилию:user.setLastName;
 Получить логин: user.getLogin();
 `);

function createNewUser() {
        let newUser = {
        firstName : prompt('Введите имя' )|| 'Ivan',
        lastName : prompt('Введите фамилию')|| 'Kravchenko',
            getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
    };
    Object.defineProperties(newUser, {
        'firstName': {
            writable: false
        },
        'lastName': {
            writable: false
        },
        setFirstName: {
            set(fName) {
                Object.defineProperty(this, 'firstName', {
                    value: fName
                });
            }
        },
        setLastName: {
            set(lName) {
                Object.defineProperty(this, 'lastName', {
                    value: lName
                });
            }
        }
    });
    return newUser;
}
let user = createNewUser();
for (let key in user){
    if (typeof user[key]!=="function"){
        console.log(`${key} - ${user[key]}`);
    }
}
console.log(`login - ${user.getLogin()}`);


