function createNewUser() {
    let newUser = {
        firstName : prompt('Введите имя' )|| 'Ivan',
        lastName : prompt('Введите фамилию')|| 'Kravchenko',
        birthday : prompt('Введите дату рождения (в формате dd.mm.yyyy)')|| '13.03.1992',
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function () {
            let dateBd = new Date(Date.parse(this.birthday.split('.').reverse().join('-')));
            dateBd.setHours(0,0,0,0);
            let dateTd = new Date();
            dateTd.setHours(0,0,0,0);
            return Math.floor((dateTd-dateBd)/31536000000);
        },
        getPassword:  function () {
          /* который будет возвращать первую букву имени пользователя в верхнем регистре,
          соединенную с фамилией (в нижнем регистре) и годом рождения. (например,
          Ivan Kravchenko 13.03.1992 → Ikravchenko1992).*/

          let passUser = this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+(this.birthday.split('.'))[2];
          return passUser;
        }
    };
    Object.defineProperties(newUser, {
        'firstName': {
            writable: false
        },
        'lastName': {
            writable: false
        },
        setFirstName: {
            set(fName) {
                Object.defineProperty(this, 'firstName', {
                    value: fName
                });
            }
        },
        setLastName: {
            set(lName) {
                Object.defineProperty(this, 'lastName', {
                    value: lName
                });
            }
        }
    });
    return newUser;
}
let user = createNewUser();
for (let key in user){
    if (typeof user[key]!=="function"){
        console.log(`${key} - ${user[key]}`);
    }
}
console.log(`login - ${user.getLogin()}`);
console.log(`Age - ${user.getAge()}`);
console.log(`Password - ${user.getPassword()}`);