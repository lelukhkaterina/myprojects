let mass = [121, 'Hello', 'hello', 'world', 23, '23', null, true, undefined, 1234567890123456789012345678901234567890n];
let typeF='undefined';

function filterBy(massEl, typeFilter) {
 return massEl.filter(function (item) {
   return typeof(item)!==typeFilter;
 });
}

console.log(`Исходный массив:`);
console.log(mass);
console.log(`Отфильтрованный массив по типу - "${typeF}":`);
console.log(filterBy(mass, typeF));