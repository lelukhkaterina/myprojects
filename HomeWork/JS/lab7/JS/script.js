window.onload = () => {
    let myMass = ['hello', [1, 2,['Kiev', 'Kharkiv',[1], 'Odessa'], 3, 'sea', 'user', 23], 'world','Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

//Функция построения списка из массива.
    function createListNew(mass) {
        let ul = document.createElement('ul');
        let newMass = mass.map(function (item, index) {
            let li = document.createElement("li");
            if (Array.isArray(item)) {
                li.append(createListNew(item));
                return li;
            } else {
                li.innerHTML = item;
                return li;
            }
        });
        console.log(newMass);
        newMass.forEach(item => ul.append(item));
        return ul;
    }

//Вывод таймера на экран.

    // Создаем элемент и присваеваем свойства.
    let div = document.createElement('div');
    div.style.cssText = `
    color: red;
    font-size:50px;
    position:fixed;
    right:50px;
    top:50px;
    text-alight:center;
    `;
    // Выводим информацию о таймере, а затем очищаем экран.
    document.body.append(div);
    let i = 10;
    let timerId = setInterval(()=>{
        div.innerHTML = `До очистки страницы осталось ... ${i}`;
        console.log(i);
        if (i===0){
            clearInterval(timerId);
            document.body.innerHTML='';
        }
        i--;
    }, 1000);

    document.body.append(createListNew(myMass)); //Вызов функции построения списка из массива.
};