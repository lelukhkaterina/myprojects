// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для
// ввода числовых значений
// Поведение поля должно быть следующим:
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен
//     текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри
//     поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем
//     выводить фразу - Please enter correct price. span со значением при этом не создается.

window.onload = () => {
    let inputEl = document.querySelector("input");

    let labelEl = document.querySelector("label");

    let errorEl = document.createElement("span");
        errorEl.classList.add("errorSpan","spanVisible");
        errorEl.innerHTML='Число введено не верно! Введите <u>положительное</u> число, больше 0!';

    let resultEl = document.createElement("span");
        resultEl.classList.add("resultSpan","spanVisible");

    let crossEl = document.createElement("button");
        crossEl.innerHTML=`&#10060 `;
        crossEl.classList.add("cross");

//фокусировка инпута
    inputEl.addEventListener("focus",
        function greenBorder() {
            inputEl.classList.add("borderInputActive");
            inputEl.classList.remove("borderInputError");
            errorEl.classList.add("spanVisible");
            resultEl.classList.add("spanVisible"); // Спрячет вывод сообщения для текущей цены, при установке фокуса повторно.
        },{passive:true});

//сброс фокуса и вывод на экран
    inputEl.addEventListener("blur",
        function getValueInput() {
            inputEl.classList.remove("borderInputActive");
            let valueInputEl=Number(inputEl.value);
            console.log(valueInputEl);
            if (isNaN(valueInputEl) || valueInputEl<=0 || inputEl.value===''){
                inputEl.classList.add("borderInputError");
                errorEl.classList.remove("spanVisible");
                labelEl.after(errorEl);
            }
            else{
                resultEl.innerHTML=`Текущая цена, $: ${valueInputEl}`;
                resultEl.classList.remove("spanVisible");
                labelEl.before(resultEl);
                resultEl.append(crossEl);
            }
        }
    );

//Реакция на нажатие крестика
    crossEl.addEventListener("click",function () {
            resultEl.classList.add("spanVisible");
            inputEl.value='';
    });

};