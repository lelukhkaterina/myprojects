//     В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для
// нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться
// для какой вкладки.
//     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//     Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом
// нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

//menu
const ulElTabs=document.querySelector(".tabs");
const liElTitle=document.querySelectorAll(".tabs-title");
//content
const ulElContent=document.querySelector(".tabs-content");
const liElContent=ulElContent.querySelectorAll("li");
//Вешаем событие на ul
ulElTabs.addEventListener("click", (event)=>{
    //Снятие активного меню
    liElTitle.forEach((item)=>{
        item.classList.remove("active");
    });
    //Отлавливаем элемент на который нажали
    let liELClick=event.target;
    //Делаем элемент активным (Добавляем класс active)
    liELClick.classList.add("active");
    //Отображаем текст вкладки
    for (let i=0; i<liElContent.length;i++){
        if (liElContent[i].dataset.info!==liELClick.innerHTML){
            liElContent[i].classList.add("none");
        }
        else liElContent[i].classList.remove("none");
    }
});
