import React, {Component} from 'react';
// import logo from './logo.svg';
import './App.scss';
import './components/Button/Button.js'
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal.js";


class App extends Component{
    state={
        modalFirstOn:false,
        modalSecondOn:false,
        buttonOn:true,
    };
    render() {

        return (
            <div className="App">
                {this.state.buttonOn &&
                    <>
                        <Button text={'Open first modal'} className={'styleButton'} functionButton={this.modalWindow} backgroundColor={'red'}/>
                        <Button text={'Open second modal'} className={'styleButton'} functionButton={this.modalWindow2} backgroundColor={'green'}/>
                    </>
                }

                {this.state.modalFirstOn &&
                    <Modal
                       header={'Lorem ipsum dolor sit amet'}
                       closeButton={true}
                       text={'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'}
                       actions={
                           <>
                               <Button functionButton={this.closeModalWindow} text={'Ok'}  className={'styleButton'} />
                               <Button functionButton={this.closeModalWindow} text={'Cancel'}  className={'styleButton'} />
                           </>
                       }
                       functionButton={this.closeModalWindow}
                    />
                }

                {this.state.modalSecondOn &&
                <Modal
                    header={'Lorem ipsum'}
                    closeButton={false}
                    backgroundColor={'#CD5C5C'}
                    text={'Nunc lobortis mattis aliquam faucibus purus in. Enim tortor at auctor urna nunc id cursus. Faucibus turpis in eu mi bibendum. Adipiscing tristique risus nec feugiat in fermentum posuere urna. Interdum varius sit amet mattis vulputate enim nulla aliquet porttitor. Sed adipiscing diam donec adipiscing. Vel turpis nunc eget lorem. Dui id ornare arcu odio ut. Blandit volutpat maecenas volutpat blandit aliquam etiam. Consectetur adipiscing elit pellentesque habitant morbi tristique senectus et netus. Egestas integer eget aliquet nibh. Dolor sit amet consectetur adipiscing elit pellentesque habitant. Congue mauris rhoncus aenean vel elit scelerisque.'}
                    actions={
                        <>
                            <Button functionButton={this.closeModalWindow} text={'Ok'}  className={'styleButton'} backgroundColor={'lightgrey'}/>
                            <Button functionButton={this.closeModalWindow} text={'Cancel'}  className={'styleButton'} backgroundColor={'darkgrey'} />
                        </>
                    }
                    functionButton={this.closeModalWindow}
                />
                }
            </div>
        )
    }

    modalWindow = () =>{
        console.log('ok');
        this.setState({modalFirstOn:true});
        // this.setState({buttonOn:false});
    };

    modalWindow2 = () =>{
        console.log('ok2');
        this.setState({modalSecondOn:true});
        // this.setState({buttonOn:false});
    };

    closeModalWindow = () =>{
        console.log('close');
        this.setState({modalFirstOn:false});
        this.setState({modalSecondOn:false});
        // this.setState({buttonOn:true});
    }
}

export default App;
