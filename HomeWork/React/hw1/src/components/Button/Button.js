import React, {Component} from 'react';
import "./StyleButton.scss"

class Button extends Component {
    render() {
        const { text, functionButton, className, backgroundColor } = this.props;

        return (
            <>
                <button  className={className} onClick={functionButton} style={{backgroundColor}}>
                    {text}
                </button>
            </>
        );
    }
}

export default Button;