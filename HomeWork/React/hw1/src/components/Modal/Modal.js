import React, {Component} from 'react';
import Button from "../Button/Button";
import './StyleModal.scss'

class Modal extends Component {
    render() {
        const {header, closeButton, text, actions, functionButton, backgroundColor}=this.props;
        return (
            <div className={'backgroundStyle'} onClick ={functionButton}>
                <div className={'windowStyle'} onClick ={this.handleClick} style={{backgroundColor}}>
                    {closeButton ? <Button functionButton={functionButton} text={'X'} className={'styleButtonX'}/>: ''}
                    <h2>{header}</h2>
                    <hr/>
                    <p>{text}</p>
                    <div className={'actionStyle'}>{actions}</div>
                </div>
            </div>
        );
    }
    handleClick(evt) {
        evt.stopPropagation()
    }
}

export default Modal;