import React, {Component}  from 'react';
// import logo from './logo.svg';
import './App.scss';
import axios from 'axios';
import Header from "./components/Header/Header";
import Body from "./components/Body/Body";


class App extends Component {
    state = {
        products: [],
        favorites: [],
        cart: [],

    };

    componentDidMount() {
        axios('/products.json')
            .then(res => this.setState({products: res.data}));

        const favoritesLS =JSON.parse(localStorage.getItem('favorites'))||[];
        const cartLS=JSON.parse(localStorage.getItem('cart'))||[];

        this.setState({favorites: favoritesLS});
        this.setState({cart: cartLS});

    }

    render() {
        const {products, favorites, cart} = this.state;
        return (
            <div className={'content'}>
                <Header favorites={favorites} cart={cart}/>
                <Body
                    products={products}
                    functionFavorites={this.favoritesClickEl}
                    functionCart={this.cartAddMassEl}
                    cart={cart}
                    favorites={favorites}
                />


            </div>

        );
    }

    favoritesClickEl = (id) => {
        const {favorites} = this.state;
        const isIncludes = favorites.includes(id);
        let newFavorite;
        if (!isIncludes) {
            newFavorite = favorites.concat(id);

        } else {
            newFavorite = favorites.filter(item => item !== id);

        }
        this.setState({favorites: newFavorite});
        localStorage.setItem('favorites', JSON.stringify(newFavorite));
    };



    cartAddMassEl=(id)=>{

        const {cart} = this.state;
        const isIncludes = cart.includes(id);
        let newCart;

        if (!isIncludes) {
            newCart = cart.concat(id);
        } else {
            newCart = cart.filter(item => item !== id);
        }
        this.setState({cart: newCart});
        console.log(newCart);
        localStorage.setItem('cart', JSON.stringify(newCart));

    };










}


export default App;
