import React, {Component} from 'react';
import Quantity from "../Quantity/Quantity";

class AllFavorite extends Component {
    render() {
        const {iconStyle, favorites}=this.props;
        return (
            <div className={'allFavoriteStyle'}>
                <img src={'./imgSite/favorites.png'}  alt="logo" style={iconStyle}/>
                {(favorites.length!==0)&&<Quantity massEl={favorites}/>}

            </div>
        );
    }
}

export default AllFavorite;