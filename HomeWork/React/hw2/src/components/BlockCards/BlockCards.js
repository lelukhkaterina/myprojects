import React, {Component} from 'react';
import ItemCards from "../ItemCards/ItemCards";
import './BlockCards.scss'

class BlockCards extends Component {
    render() {
        const {products, functionFavorites, functionCart, cart, favorites}=this.props;
        const productsItems = products.map((item) =>
            <ItemCards
                key={item.id}
                productsItem={item}
                functionFavorites={functionFavorites}
                functionCart={functionCart}
                cart={cart}
                favorites={favorites}
                favoritesAdd={this.elAdd(item.id, favorites)}
                cartAdd={this.elAdd(item.id, cart)}
            />
        );

        return (
            <div className={'blockCardsStyle'}>
                {productsItems}
            </div>
        );
    }

    elAdd = (id, onMass) => onMass.includes(id);



}

export default BlockCards;