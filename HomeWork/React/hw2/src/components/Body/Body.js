import React, {Component} from 'react';
import BlockCards from "../BlockCards/BlockCards";


class Body extends Component {

    render() {
        const {products, functionFavorites, functionCart, cart, favorites}=this.props;

        return (
            <div>
                <BlockCards
                    products={products}
                    functionFavorites={functionFavorites}
                    functionCart={functionCart}
                    cart={cart}
                    favorites={favorites}
                />
            </div>
        );
    }

}

export default Body;