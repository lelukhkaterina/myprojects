import React, {Component} from 'react';
import "./StyleButton.scss"

class Button extends Component {
    render() {
        const { text, functionOnClick, className, backgroundColor,productItemId } = this.props;

        return (
            <div className={'buttonElStyle'} >
                <button
                    className={className}
                    onClick={()=>functionOnClick(productItemId)}
                style={{backgroundColor}}>
                    {text}
                </button>
            </div>
        );
    }
}

export default Button;