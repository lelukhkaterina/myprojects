import React, {Component} from 'react';


class Favorites extends Component {

    render() {
        const {iconStyle, functionFavorites, productItemId, favoritesAdd}=this.props;
        const imgUrl = './imgSite/favorites.png';
        const imgUrlYellow='./imgSite/favoritesyellow.png';

        return (
            <div onClick={()=>functionFavorites(productItemId)}>
                {!favoritesAdd && <img src={imgUrl}  alt="logo" style={iconStyle}/>}
                {favoritesAdd && <img src={imgUrlYellow}  alt="logo" style={iconStyle}/>}
            </div>
        );

    }
}

export default Favorites;