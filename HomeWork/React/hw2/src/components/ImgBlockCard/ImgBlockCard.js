import React, {Component} from 'react';
import "./ImgBlockCard.scss"

class ImgBlockCard extends Component {
    render() {
        const {productsItemsImg}=this.props;


        return (
            <div className={'imgBlockCardStyle'}>
                <img src={productsItemsImg} alt="imgProducts"/>
            </div>
        );
    }
}

export default ImgBlockCard;