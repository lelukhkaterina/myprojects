import React, {Component} from 'react';
import "./ItemCards.scss"
import ImgBlockCard from "../ImgBlockCard/ImgBlockCard";
import NameBlockCard from "../NameBlockCard/NameBlockCard";
import PriceBlockCard from "../PriceBlockCard/PriceBlockCard";
import Favorites from "../Favorites/Favorites";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";

class ItemCards extends Component {

    state={
        modal:false,
    };

    render() {
        const {
            productsItem,
            favoritesAdd,
            functionFavorites,
            cartAdd
        }=this.props;
        const {
            modalOn,
            modalRemoveOn,
            modal
        }=this.state;

        const iconStyle={
            width:'30px',
            height:'30px',
            position: 'absolute',
            right:'10px',
            top:'10px',
            zIndex:'2',
            cursor:'pointer',
        };
        const text=(cartAdd)?  'Удалить товар из корзины':'Добавить товар в корзину';

        return (
            <div className={'itemCardsStyle'}>
                <Favorites
                    iconStyle={iconStyle}
                    functionFavorites={functionFavorites}
                    productItemId={productsItem.id}
                    favoritesAdd={favoritesAdd}
                />
                <ImgBlockCard productsItemsImg={productsItem.url}/>
                <NameBlockCard productItemName={productsItem.name}/>
                <PriceBlockCard productItemPrice={productsItem.price}/>
                <Button
                    functionOnClick={this.cartAddClickEl}
                    productItemId={productsItem.id}
                    text={text}
                    className={'styleButton'}
                    backgroundColor={'green'}
                />

                {modal &&
                    <Modal
                    header={'Корзина'}
                    text={`${text}?`}
                    modalOk={this.modalOk}
                    productItemId={productsItem.id}
                    closeModalWindow={this.closeModalWindow}
                    />
                }
            </div>
        );
    }

    cartAddClickEl = () => {
       this.setState({modal:true});
    };

    closeModalWindow = () =>{
        this.setState({modal:false});
    };

    modalOk= () =>{
        const {functionCart, productsItem}=this.props;
        functionCart(productsItem.id);
        this.closeModalWindow();
    };
}

export default ItemCards;