import React, {Component} from 'react';
import './Logo.scss'

class Logo extends Component {
    render() {
        const {style}= this.props;
        return (
            <div>
                <img src={'./imgSite/logo2.png'}  alt="logo" style={style} />
            </div>
        );
    }
}

export default Logo;