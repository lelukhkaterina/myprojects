import React, {Component} from 'react';
import './Menu.scss'


class Menu extends Component {
    render() {
        const{itemMenu}=this.props;
        const listItems = itemMenu.map((itemMenu) =>
            <li key={itemMenu.id}><a href={itemMenu.link}>{itemMenu.itemName}</a></li>
        );

        return (
            <div >
                <ul className={'menuStyle'}>{listItems}</ul>

            </div>
        );
    }
}

export default Menu;