import React, {Component} from 'react';
import Button from "../Button/Button";
import './StyleModal.scss'

class Modal extends Component {
    render() {
        const {
            header,
            closeButton,
            text,
            functionButton,
            backgroundColor,
            modalOk,
            productItemId,
            closeModalWindow
        }=this.props;


        return (
            <div className={'backgroundStyle'} onClick ={functionButton}>
                <div className={'windowStyle'} onClick ={this.handleClick} style={{backgroundColor}}>
                    {closeButton ? <Button functionButton={functionButton} text={'X'} className={'styleButtonX'}/>: ''}
                    <h2>{header}</h2>
                    <hr/>
                    <p>{text}</p>
                    <div className={'buttonBlock'}>
                        <Button
                            text={'Ok'}
                            functionOnClick={modalOk}
                            productItemId={productItemId}
                            className={"styleButton"}
                        />
                        <Button
                            text={'Close'}
                            functionOnClick={closeModalWindow}
                            className={"styleButton"}
                        />
                    </div>
                </div>
            </div>
        );
    }
    handleClick(evt) {
        evt.stopPropagation()
    }
}

export default Modal;