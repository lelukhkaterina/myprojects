import React, {Component} from 'react';
import "./NameBlockCard.scss"


class NameBlockCard extends Component {
    render() {
        const {productItemName}=this.props;
        return (
            <div className={"nameBlockCardStyle"}>
                {productItemName}
            </div>
        );
    }
}

export default NameBlockCard;