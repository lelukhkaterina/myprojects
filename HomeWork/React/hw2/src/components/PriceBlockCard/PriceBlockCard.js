import React, {Component} from 'react';
import "./PriceBlockCard.scss"

class PriceBlockCard extends Component {
    render() {
        const {productItemPrice}=this.props;
        return (
            <div className={'priceBlockCardStyle'}>
                {productItemPrice}
            </div>
        );
    }
}

export default PriceBlockCard;