import React, {Component} from 'react';
import "./Quantity.scss"

class Quantity extends Component {
    render() {
        const {massEl}=this.props;
        return (
            <div>
                <div className={'quantityStyle'} >{massEl.length}</div>
            </div>
        );
    }
}

export default Quantity;