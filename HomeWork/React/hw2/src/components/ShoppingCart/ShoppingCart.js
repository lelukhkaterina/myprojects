import React, {Component} from 'react';
import Quantity from "../Quantity/Quantity";


class ShoppingCart extends Component {
    render() {
        const {iconStyle, cart}=this.props;
        return (
            <div>
                <img src={'./imgSite/shopping-cart.png'}  alt="logo" style={iconStyle}/>
                {(cart.length!==0)&&<Quantity massEl={cart}/>}
            </div>
        );
    }
}

export default ShoppingCart;