import React, {Component} from 'react';
import './Sidebar.scss'
import ShoppingCart from "../ShoppingCart/ShoppingCart";
import AllFavorite from "../AllFavorite/AllFavorite";

class Sidebar extends Component {
    render() {
        const {favorites, cart}=this.props;
        const iconStyle={
            width:'40px',
            height:'40px',
            padding:'5px',
            cursor:'pointer',

        };
        return (
            <div className={'sideBar'}>
                <a href={'#'}>
                    <img src={'./imgSite/user.png'}
                         alt="logo"
                         style={iconStyle}
                    />
                </a>
                <AllFavorite
                    iconStyle={iconStyle}
                    favorites={favorites}
                />
                <ShoppingCart
                    iconStyle={iconStyle}
                    cart={cart}
                />
            </div>
        );
    }
}

export default Sidebar;