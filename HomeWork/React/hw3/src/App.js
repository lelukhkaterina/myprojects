import React, { useState, useEffect } from 'react';
import './App.scss';
import axios from 'axios';
import Header from "./components/Header/Header";
import Body from "./components/Body/Body";



function App (props) {
    const [products, setProducts] = useState([]);
    const [favorites, setFavorites] = useState([]);
    const [cart, setCart] = useState([]);

    //Рендерится в начале, один раз.

    useEffect(() => {
        axios('/products.json')
            .then(res => setProducts(res.data));
        const favoritesLS =JSON.parse(localStorage.getItem('favorites'))||[];
        const cartLS=JSON.parse(localStorage.getItem('cart'))||[];
        setFavorites(favoritesLS);
        setCart(cartLS);
    }, []);

    const favoritesClickEl = (id) => {
        const isIncludes = favorites.includes(id);
        let newFavorite;
        if (!isIncludes) {
            newFavorite = favorites.concat(id);
        } else {
            newFavorite = favorites.filter(item => item !== id);
        }
        setFavorites(newFavorite);
        localStorage.setItem('favorites', JSON.stringify(newFavorite));
    };



    const cartAddMassEl=(id)=>{
        const isIncludes = cart.includes(id);
        let newCart;
        if (!isIncludes) {
            newCart = cart.concat(id);
        } else {
            newCart = cart.filter(item => item !== id);
        }
        setCart(newCart);
        localStorage.setItem('cart', JSON.stringify(newCart));
    };


    return (
        <div className={'content'}>
            <Header favorites={favorites} cart={cart}/>
            <Body
                products={products}
                functionFavorites={favoritesClickEl}
                functionCart={cartAddMassEl}
                cart={cart}
                favorites={favorites}
            />

        </div>
    );
}



export default App;
