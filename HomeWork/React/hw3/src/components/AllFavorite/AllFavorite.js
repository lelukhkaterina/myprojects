import React  from 'react';
import Quantity from "../Quantity/Quantity";


function AllFavorite(props) {
    return (
        <div className={'allFavoriteStyle'}>
                <img src={'./imgSite/favorites.png'}  alt="logo" style={props.iconStyle}/>
                {(props.favorites.length!==0)&&<Quantity massEl={props.favorites}/>}
        </div>
    );
}

export default AllFavorite;