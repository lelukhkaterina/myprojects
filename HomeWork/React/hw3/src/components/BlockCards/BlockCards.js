import React from 'react';
import ItemCards from "../ItemCards/ItemCards";
import './BlockCards.scss'

function BlockCards (props) {
    const productsItems = props.products.map((item) =>
        <ItemCards
            key={item.id}
            productsItem={item}
            functionFavorites={props.functionFavorites}
            functionCart={props.functionCart}
            cart={props.cart}
            favorites={props.favorites}
            favoritesAdd={elAdd(item.id, props.favorites)}
            cartAdd={elAdd(item.id, props.cart)}
            crossOn={props.crossOn}
        />
    );
    function elAdd(id, onMass) {
        return onMass.includes(id)
    }

    return (
            <div className={'blockCardsStyle'}>
                {/*{(productsItems.length === 0) && 'Товары отсутствуют'}*/}
                {productsItems}
            </div>
        );

}






export default BlockCards;