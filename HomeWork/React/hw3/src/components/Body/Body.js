import React from 'react';
import BlockCards from "../BlockCards/BlockCards";
import FavoritesBlockCards from "../FavoritesBlockCards/FavoritesBlockCards";
import CartBlockCards from "../CartBlockCards/CartBlockCards";
import { Route, Switch } from "react-router-dom";

function Body(props) {
    return (
        <div>
            <Switch>

                <Route exact path='/' render={(routerProps) =>
                    <BlockCards
                        products={props.products}
                        functionFavorites={props.functionFavorites}
                        functionCart={props.functionCart}
                        cart={props.cart}
                        favorites={props.favorites}
                    />}/>

                <Route exact path='/favorites' render={(routerProps) =>
                    <FavoritesBlockCards
                        products={props.products}
                        functionFavorites={props.functionFavorites}
                        functionCart={props.functionCart}
                        cart={props.cart}
                        favorites={props.favorites}
                    />}/>

                <Route exact path='/cart' render={(routerProps) =>
                    <CartBlockCards
                        products={props.products}
                        functionFavorites={props.functionFavorites}
                        functionCart={props.functionCart}
                        cart={props.cart}
                        favorites={props.favorites}
                    />}/>
            </Switch>
      </div>
    );
}

export default Body;