import React from 'react';
import BlockCards from "../BlockCards/BlockCards";

function CartBlockCards(props) {
    const shCart = props.products.filter((item) =>
        props.cart.includes(item.id)
    );

    return (
        <div>
            {(shCart.length === 0) && 'Корзина пуста'}
            <BlockCards
                products={shCart}
                functionFavorites={props.functionFavorites}
                functionCart={props.functionCart}
                cart={props.cart}
                favorites={props.favorites}
                crossOn={true}
            />
        </div>
    );
}

export default CartBlockCards;