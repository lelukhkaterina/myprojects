import React from 'react';

function Favorites(props) {
    const imgUrl = './imgSite/favorites.png';
    const imgUrlYellow='./imgSite/favoritesyellow.png';

    return (
        <div onClick={()=>props.functionFavorites(props.productItemId)}>
            {!props.favoritesAdd && <img src={imgUrl}  alt="logo" style={props.iconStyle}/>}
            {props.favoritesAdd && <img src={imgUrlYellow}  alt="logo" style={props.iconStyle}/>}
        </div>
    );
}


export default Favorites;