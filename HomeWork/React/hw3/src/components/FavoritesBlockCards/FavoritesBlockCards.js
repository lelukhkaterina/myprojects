import React from 'react';
import BlockCards from "../BlockCards/BlockCards";


function FavoritesBlockCards(props) {

    const shFavorites = props.products.filter((item) =>
        props.favorites.includes(item.id)
    );

    return (
        <div>
            {(shFavorites.length === 0) && 'Избранные товары отсутствуют'}
            <BlockCards
                products={shFavorites}
                functionFavorites={props.functionFavorites}
                functionCart={props.functionCart}
                cart={props.cart}
                favorites={props.favorites}
            />
        </div>
    );
}

export default FavoritesBlockCards;