import React from 'react';
import Logo from "../Logo/Logo";
import Menu from "../Menu/Menu";
import Sidebar from "../Sidebar/Sidebar";
import './Header.scss'


const styleLogo={
    width:'200px',
    height:'100px',
};


const itemMenu=[
    {
        id:1,
        itemName:'* Home',
        link:'/'
    },
    {
        id:2,
        itemName:'* Кателог товаров',
        link:'/'
    },
    {
        id:3,
        itemName:'* Контакты',
        link:'/'
    },
    {
        id:4,
        itemName:' * Помощь',
        link:'/'
    },
    {
        id:5,
        itemName: '* Обратный звонок',
        link:'/'
    }
];

function Header(props) {
    return (
        <div>
            <div className={'headerStyle'}>
                <Logo style={styleLogo}/>
                <Menu itemMenu={itemMenu}/>
                <Sidebar
                    favorites={props.favorites}
                    cart={props.cart}
                />
            </div>
            <hr/>
            <img src={'./imgSite/imgShop.jpg'} className='imgShop' alt="imgShop"/>
            <hr/>
        </div>
    );
}


export default Header;