import React, {useState} from 'react';
import "./ItemCards.scss"
import ImgBlockCard from "../ImgBlockCard/ImgBlockCard";
import NameBlockCard from "../NameBlockCard/NameBlockCard";
import PriceBlockCard from "../PriceBlockCard/PriceBlockCard";
import Favorites from "../Favorites/Favorites";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";

function ItemCards(props) {

    const [modal, setModal] = useState(false);
    const iconStyle = {
        width: '30px',
        height: '30px',
        position: 'absolute',
        right: '10px',
        top: '10px',
        zIndex: '2',
        cursor: 'pointer',
    };
    const text = (props.cartAdd) ? 'Удалить товар из корзины' : 'Добавить товар в корзину';
    const cartAddClickEl = () => {
        setModal(true);
    };
    const closeModalWindow = () => {
        setModal(false);
    };
    const modalOk = () => {
        props.functionCart(props.productsItem.id);
        closeModalWindow();
    };

    return (
        <div className={'itemCardsStyle'}>
            <Favorites
                iconStyle={iconStyle}
                functionFavorites={props.functionFavorites}
                productItemId={props.productsItem.id}
                favoritesAdd={props.favoritesAdd}
            />

            {props.crossOn &&
            <Button
                functionOnClick={cartAddClickEl}
                productItemId={props.productsItem.id}
                text={'X'}
                className={'styleButtonX'}
                backgroundColor={'red'}
            />

            }
            <ImgBlockCard productsItemsImg={props.productsItem.url}/>
            <NameBlockCard productItemName={props.productsItem.name}/>
            <PriceBlockCard productItemPrice={props.productsItem.price}/>
            <Button
                functionOnClick={cartAddClickEl}
                productItemId={props.productsItem.id}
                text={text}
                className={'styleButton'}
                backgroundColor={'green'}
            />

            {modal &&
            <Modal
                header={'Корзина'}
                text={`${text}?`}
                modalOk={modalOk}
                productItemId={props.productsItem.id}
                closeModalWindow={closeModalWindow}
            />
            }
        </div>
    );
}


export default ItemCards;