import React from 'react';
import "./PriceBlockCard.scss"

function PriceBlockCard (props){
        return (
            <div className={'priceBlockCardStyle'}>
                {props.productItemPrice}
            </div>
        );
}


export default PriceBlockCard;