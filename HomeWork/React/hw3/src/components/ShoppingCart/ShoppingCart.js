import React from 'react';
import Quantity from "../Quantity/Quantity";

function ShoppingCart(props) {
    return (
        <div>
            <img src={'./imgSite/shopping-cart.png'}  alt="logo" style={props.iconStyle}/>
            {(props.cart.length!==0)&&<Quantity massEl={props.cart}/>}
        </div>
    );
}

export default ShoppingCart;