import React from 'react';
import "./StyleButton.scss"

function Button(props) {
    return (
        <div className={'buttonElStyle'} >
            <button
                className={props.className}
                onClick={()=>props.functionOnClick(props.productItemId)}
                style={{backgroundColor: props.backgroundColor}}>
                {props.text}
            </button>
        </div>
    );
}

export default Button;