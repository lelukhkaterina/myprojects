import React from 'react';
import BlockCards from "../BlockCards/BlockCards";
import {connect} from "react-redux";


function CartBlockCards(props) {
    const shCart = props.products.filter((item) =>
        props.cart.includes(item.id)
    );
console.log(shCart);
    return (
        <div>
            {(shCart.length === 0) && 'Корзина пуста'}
            <BlockCards
                products={shCart}
                crossOn={true}
            />
        </div>
    );
}

function mapStateToProps(state) {
    return{
        products: state.initInfo.products,
        cart: state.initInfo.cart,
    }
}

export default connect(mapStateToProps)(CartBlockCards);