import React from 'react';
import "./ItemCards.scss"
import ImgBlockCard from "../ImgBlockCard/ImgBlockCard";
import NameBlockCard from "../NameBlockCard/NameBlockCard";
import PriceBlockCard from "../PriceBlockCard/PriceBlockCard";
import Favorites from "../Favorites/Favorites";
import Button from "../Button/Button";
import {setModalAction, setIdAction, setTextAction} from "../../actions/actionProdacts";
import {connect} from "react-redux";

function ItemCards (props) {

    const text = (props.cartAdd) ? 'Удалить товар из корзины' : 'Добавить товар в корзину';
    const cartAddClickEl = () => {
        props.setModalAction(true);
        props.setTextAction(text);
        props.setIdAction(props.productsItem.id);
    };

    return (
        <div className={'itemCardsStyle'}>
            <Favorites
                id={props.productsItem.id}
                favoritesAdd={props.favoritesAdd}
            />

            {props.crossOn &&
            <Button
                functionOnClick={cartAddClickEl}
                productItemId={props.productsItem.id}
                text={'X'}
                className={'styleButtonX'}
                backgroundColor={'red'}
            />

            }
            <ImgBlockCard productsItemsImg={props.productsItem.url}/>
            <NameBlockCard productItemName={props.productsItem.name}/>
            <PriceBlockCard productItemPrice={props.productsItem.price}/>
            <Button
                functionOnClick={cartAddClickEl}
                productItemId={props.productsItem.id}
                text={text}
                className={'styleButton'}
            />
        </div>
    );

}


function mapStateToProps(state) {
    return{
        modal: state.initInfo.modal,
        text: state.initInfo.text
    }

}
const  mapDispatchToProps =
    {
        setModalAction,
        setIdAction,
        setTextAction,
    };


export default connect(mapStateToProps, mapDispatchToProps)(ItemCards);


