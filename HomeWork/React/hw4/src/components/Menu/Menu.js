import React from 'react';
import './Menu.scss'
import {Link} from "react-router-dom";



function Menu(props) {
    const listItems = props.itemMenu.map((itemMenu) =>
        <li key={itemMenu.id}><Link  to={itemMenu.link}>{itemMenu.itemName}</Link></li>
    );

    return (
        <div >
            <ul className={'menuStyle'}>{listItems}</ul>
        </div>
    );
}

export default Menu;