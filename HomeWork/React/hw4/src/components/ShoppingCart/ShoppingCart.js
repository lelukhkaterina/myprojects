import React from 'react';
import Quantity from "../Quantity/Quantity";
import {connect} from "react-redux";

function ShoppingCart(props) {
    return (
        <div>
            <img src={'./imgSite/shopping-cart.png'}  alt="logo" style={props.iconStyle}/>
            {(props.cart.length!==0)&&<Quantity massEl={props.cart}/>}
        </div>
    );
}

function mapStateToProps(state) {
    return{
        cart: state.initInfo.cart,
    }
}

export default connect(mapStateToProps)(ShoppingCart);

