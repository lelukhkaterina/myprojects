import React, { useEffect } from 'react';
import './App.scss';
import Header from "./components/Header/Header";
import Body from "./components/Body/Body";
import {connect} from 'react-redux';
import {setProductsAction, setModalAction, setCartAction, setFavoritesAction} from './actions/actionProdacts';
import Modal from "./components/Modal/Modal";

function App (props) {

    //Рендерится в начале, один раз.

    useEffect(() => {
        props.setProductsAction();
        const favoritesLS =JSON.parse(localStorage.getItem('favorites'))||[];
        const cartLS=JSON.parse(localStorage.getItem('cart'))||[];
        props.setFavoritesAction(favoritesLS);
        props.setCartAction(cartLS);
    }, []);

    const closeModalWindow = () => {
        props.setModalAction(false)
    };
    const modalOk = () => {
        cartAddMassEl(props.id);
        closeModalWindow();
    };

    const cartAddMassEl=(id)=>{
        const isIncludes = props.cart.includes(id);
        let newCart;
        if (!isIncludes) {
            newCart = props.cart.concat(id);
        } else {
            newCart = props.cart.filter(item => item !== id);
        }
        props.setCartAction(newCart);
        localStorage.setItem('cart', JSON.stringify(newCart));
    };

    return (
        <div className={'content'}>
            <Header/>
            <Body/>
            {
                props.modal &&
                    <Modal
                        header={'Корзина'}
                        text={`${props.text}?`}
                        modalOk={modalOk}
                        productItemId={props.id}
                        closeModalWindow={closeModalWindow}
                    />
            }
        </div>
    );
}

function mapStateToProps(state) {
    return{
        products: state.initInfo.products,
        modal: state.initInfo.modal,
        text: state.initInfo.text,
        id: state.initInfo.id,
        cart: state.initInfo.cart,
        favorites: state.initInfo.favorites
    }
}

const mapDispatchToProps= {
       setProductsAction,
            setModalAction,
            setCartAction,
            setFavoritesAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
