import axios from "axios";

export const setProductsAction = () => (dispatch) => {
    axios('/products.json')
        .then(res => {
            dispatch({
                type: "SET_PRODUCTS",
                payload: res.data
            })
        });
};
export const setModalAction = (modalState) => (dispatch) => {
    dispatch({
        type: "SET_MODAL_STATE",
        payload: modalState
    })
};

export const setIdAction = (id) => (dispatch) => {
    dispatch({
        type: "SET_ID_STATE",
        payload: id
    })
};

export const setTextAction = (text) => (dispatch) => {
    dispatch({
        type: "SET_TEXT_STATE",
        payload: text
    })
};

export const setCartAction = (cart) => (dispatch) => {
    dispatch({
        type: "SET_CART_STATE",
        payload: cart
    })
};

export const setFavoritesAction = (favorites) => (dispatch) => {
    dispatch({
        type: "SET_FAVORITES_STATE",
        payload: favorites
    })
};




