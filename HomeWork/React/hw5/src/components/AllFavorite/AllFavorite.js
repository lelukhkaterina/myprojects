import React  from 'react';
import Quantity from "../Quantity/Quantity";
import {connect} from "react-redux";


function AllFavorite(props) {
    return (
        <div className={'allFavoriteStyle'}>
                <img src={'./imgSite/favorites.png'}  alt="logo" style={props.iconStyle}/>
                {(props.favorites.length!==0)&&<Quantity massEl={props.favorites}/>}
        </div>
    );
}

function mapStateToProps(state) {
    return{
        favorites: state.initInfo.favorites
    }

}
export default connect(mapStateToProps)(AllFavorite);


