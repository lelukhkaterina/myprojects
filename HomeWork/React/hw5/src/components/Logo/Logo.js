import React from 'react';
import './Logo.scss'

function Logo(props) {
    return (
        <div>
            <img src={'./imgSite/logo2.png'}  alt="logo" style={props.style} />
        </div>
    );
}


export default Logo;