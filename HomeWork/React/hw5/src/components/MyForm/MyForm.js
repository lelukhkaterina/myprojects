import React from 'react';
import {Formik, Form} from 'formik';
import * as yup from 'yup';
import MyInput from "../MyInut/MyInput";
import './MyForm.scss'
import {setCartAction} from "../../actions/actionProdacts";
import {connect} from "react-redux";

function MyForm(props) {

    const phoneRegExp = /^((\+38))?([ ])?((\(\d{3}\))|(\d{3}))?([ ])?(\d{3}[- ]?\d{2}[- ]?\d{2})$/;
    const FIELD_REQUIRED = 'This field is required!!!';
    const schema = yup.object().shape({
        userName: yup
            .string()
            .required(FIELD_REQUIRED),
        userLastName: yup
            .string()
            .required(FIELD_REQUIRED),
        age: yup
            .number()
            .required(FIELD_REQUIRED)
            .min(16, 'The minimum age is 16 years!')
            .max(120, 'The maximum age is 120 years!'),
        address: yup
            .string()
            .required(FIELD_REQUIRED),
        phone: yup
            .string()
            .required(FIELD_REQUIRED)
            .matches(phoneRegExp, 'Phone error!'),
    });

    const handleSubmit = (values, {setSubmitting}) => {
        const newCart = [];
        console.log('Получатель', values);
        console.log('Заказанный товар', props.products);

        setSubmitting(false);
        props.setCartAction(newCart);
        localStorage.setItem('cart', JSON.stringify(newCart));
        alert(`${values.userName}, Ваш заказ оформлен. Спасибо.`);
    };

    return (
        <div>
            <Formik
                initialValues={{
                    userName: '',
                    userLastName: '',
                    age: '',
                    address: '',
                    phone: '',
                }}
                onSubmit={handleSubmit}
                validationSchema={schema}
            >
                {(formikProps) => {
                    return (
                        <div>
                            <Form className={'myFormClass'}>
                                <legend align='center' className='colorTextLegend'><h4>Заполните данные для заказа
                                    товара. </h4></legend>
                                <MyInput
                                    label='Name'
                                    type='text'
                                    placeholder='Name'
                                    name='userName'
                                />
                                <MyInput
                                    label='Last Name'
                                    type='text'
                                    placeholder='Last Name'
                                    name='userLastName'
                                />

                                <MyInput
                                    label='Age'
                                    type='number'
                                    placeholder='age'
                                    name='age'
                                />
                                <MyInput
                                    label='Address'
                                    type='text'
                                    placeholder='address'
                                    name='address'
                                />
                                <MyInput
                                    label='Phone'
                                    type='phone'
                                    placeholder='Phone'
                                    name='phone'
                                />
                                <div className={'myFormButtonDiv'}>
                                    <button
                                        className={'myFormButton'}
                                        type='submit'
                                        disabled={formikProps.isSubmitting}>
                                        Оформить заказ
                                    </button>
                                </div>
                            </Form>
                        </div>
                    )
                }}
            </Formik>

        </div>
    );
}

const mapDispatchToProps = {setCartAction};
export default connect(null, mapDispatchToProps)(MyForm);