import React from 'react';
import "./NameBlockCard.scss"



function NameBlockCard(props) {
    return (
        <div className={"nameBlockCardStyle"}>
            {props.productItemName}
        </div>
    );
}

export default NameBlockCard;