const initialState={
    products: [],
    modal:false,
    text:'Добавить товар в корзину',
    id:'',
    cart: [],
    favorites: [],
};


export default function initInfo (state=initialState, action) {
   switch (action.type) {
       case "SET_PRODUCTS":
           return {...state, products: action.payload};

       case "SET_MODAL_STATE":
           return {...state, modal: action.payload};

       case "SET_TEXT_STATE":
           return {...state, text: action.payload};

       case "SET_ID_STATE":
           return {...state, id: action.payload};

       case "SET_CART_STATE":
           return {...state, cart: action.payload};

       case "SET_FAVORITES_STATE":
           return {...state, favorites: action.payload};

       default:
           return state;
   }
}