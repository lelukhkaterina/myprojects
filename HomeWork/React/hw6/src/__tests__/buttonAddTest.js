import React from "react";
import {render} from '@testing-library/react'
import { unmountComponentAtNode } from "react-dom";
import Modal from "../components/Modal/Modal";
import {act} from "react-dom/test-utils";
import Button from "../components/Button/Button";
// import {App} from "../App";

let container = null;
beforeEach(() => {
    // подготавливаем DOM-элемент, куда будем рендерить
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // подчищаем после завершения
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});


describe('my testing', ()=>{
    test('Render modal', () =>{
        act (() => {
            render(<Modal/>, container)
        })

    })

    test('Render button', () =>{
        // expect(sum (2,3)).toBe(5);
        act (() => {
            render(<Button/>, container)
        })


    })

})