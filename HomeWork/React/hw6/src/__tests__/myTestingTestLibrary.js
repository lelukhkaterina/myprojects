import React from "react";
import {render} from '@testing-library/react'
import Modal from "../components/Modal/Modal";
import Button from "../components/Button/Button";
import ItemCards from "../components/ItemCards/ItemCards";
import {Provider} from "react-redux";
import { screen } from '@testing-library/dom'
import store from "../store/store";


describe('my testing', ()=>{

    test('Render modal smoke test', () =>{
        render(<Modal/>)
    });

    test('Modal header text', () => {
            render(
                <Modal
                    header={'Корзина'}
                    text={`Добавить товар в корзину?`}
                />
            );
            const textHeader = document.querySelector('h2');
            console.log(textHeader.innerHTML);
            expect(textHeader.innerHTML).toBe('Корзина');
   });

    test('Render successfully modal test', () => {
        const modalStatus = true;
        {modalStatus && render(<Modal/> )}
        const modalEl =screen.getByTestId('modalWindow');
        expect(modalEl).toBeInTheDocument();

    });

    test('Render not successfully modal test', () => {
        const modalStatus = false;
        {modalStatus && render(<Modal/> )}
        const modalEl =screen.queryByTestId('modalWindow');
        expect(modalEl).toBe(null);
    });

    test('Modal button "ok" test', () => {
        const onClick = jest.fn();
        render(
            <Modal
                header={'Корзина'}
                text={`Добавить товар в корзину?`}
                modalOk={onClick}
            />
        );
        const buttonOk = document.getElementsByTagName("button")[0];
        expect(onClick).not.toHaveBeenCalled();
        buttonOk.dispatchEvent(new MouseEvent('click', {bubbles:true}))
        expect(onClick).toHaveBeenCalled();
    });

    test('Modal button "close" test', () => {
        const closeModalWindow = jest.fn();
        render(
            <Modal
                header={'Корзина'}
                text={`Добавить товар в корзину?`}
                closeModalWindow={closeModalWindow}
            />
        );
        const buttonClose = document.getElementsByTagName("button")[1];
        expect(closeModalWindow).not.toHaveBeenCalled();
        buttonClose.dispatchEvent(new MouseEvent('click', {bubbles:true}))
        expect(closeModalWindow).toHaveBeenCalled();
    });

    test('Render button smoke test', () =>{
            render(<Button/>)
    });

    test('Test button click', () =>{
        const onClick = jest.fn();
        render(
            <Button
                className={'className'}
                functionOnClick={onClick}
                style={{backgroundColor: 'green'}}
                text={'ok'}
            />
        );
        const buttonOk = document.getElementsByTagName("button")[0];
        expect(onClick).not.toHaveBeenCalled();
        buttonOk.dispatchEvent(new MouseEvent('click', {bubbles:true}))
        expect(onClick).toHaveBeenCalled();
    });

    test('Render ItemCardsEl & testing text button "add cart"', () =>{
        const prodItem={
            name: "Холодильник VESTFROST VD142RS",
            price: 3799,
            url: "./imgProducts/231245.jpg",
            id: 231245,
            color: "silver"
        };
        const {getByTestId} = render(
                <Provider store={store}>
                    <ItemCards
                        key={prodItem.id}
                        productsItem={prodItem}
                        favoritesAdd={true}
                        cartAdd={false}
                        crossOn={false}
                    />
                </Provider>
            );
        const addCartButton =screen.getByTestId('addCart');
        if (addCartButton.cartAdd){
            expect(addCartButton.innerHTML).toBe("Удалить товар из корзины");
        }
        else if (!addCartButton.cartAdd){
            expect(addCartButton.innerHTML).toBe('Добавить товар в корзину');
        }
    });
});