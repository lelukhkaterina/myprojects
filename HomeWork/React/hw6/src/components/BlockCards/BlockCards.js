import React from 'react';
import ItemCards from "../ItemCards/ItemCards";
import './BlockCards.scss'
import {connect} from "react-redux";

function BlockCards (props) {
    const productsItems = props.products.map((item) =>
        <ItemCards
            key={item.id}
            productsItem={item}
            favoritesAdd={elAdd(item.id, props.favorites)}
            cartAdd={elAdd(item.id, props.cart)}
            crossOn={props.crossOn}
        />
    );

    function elAdd(id, onMass) {
        return onMass.includes(id)
    }

    return (
            <div className={'blockCardsStyle'}>
                {productsItems}
            </div>
        );

}

function mapStateToProps(state) {
    return{
        cart: state.initInfo.cart,
        favorites: state.initInfo.favorites
    }

}

export default connect(mapStateToProps)(BlockCards);



