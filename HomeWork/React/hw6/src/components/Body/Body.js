import React from 'react';
import BlockCards from "../BlockCards/BlockCards";
import FavoritesBlockCards from "../FavoritesBlockCards/FavoritesBlockCards";
import CartBlockCards from "../CartBlockCards/CartBlockCards";
import {Route, Switch} from "react-router-dom";
import {connect} from "react-redux";

function Body(props) {
    return (
        <div>
            <Switch>
                <Route exact path='/'>
                    <BlockCards products={props.products} />
                </Route>
                <Route exact path='/favorites'>
                    <FavoritesBlockCards/>
                </Route>
                <Route exact path='/cart'>
                    <CartBlockCards/>
                </Route>
            </Switch>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        products: state.initInfo.products
    }
}

export default connect(mapStateToProps)(Body);