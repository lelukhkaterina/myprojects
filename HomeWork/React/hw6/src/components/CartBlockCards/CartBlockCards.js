import React from 'react';
import BlockCards from "../BlockCards/BlockCards";
import {connect} from "react-redux";
import Form from "../MyForm/MyForm";



function CartBlockCards(props) {
    const shCart = props.products.filter((item) =>
        props.cart.includes(item.id)
    );
// console.log(shCart);
    return (
        <div>
            {(shCart.length === 0) && 'Корзина пуста'}
            {(shCart.length !== 0) &&
                <div>
                    <BlockCards
                        products={shCart}
                        crossOn={true}
                    />
                    <Form
                        products={shCart}
                    />
                </div>
            }
        </div>
    );
}

function mapStateToProps(state) {
    return{
        products: state.initInfo.products,
        cart: state.initInfo.cart,
    }
}

export default connect(mapStateToProps)(CartBlockCards);