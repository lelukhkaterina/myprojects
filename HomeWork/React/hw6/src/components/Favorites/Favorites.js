import React from 'react';
import {setFavoritesAction} from "../../actions/actionProdacts";
import {connect} from "react-redux";

function Favorites(props) {
    const imgUrl = './imgSite/favorites.png';
    const imgUrlYellow='./imgSite/favoritesyellow.png';
    const iconStyle={
        width:'30px',
        height:'30px',
        position: 'absolute',
        right:'10px',
        top:'10px',
        zIndex:'2',
        cursor:'pointer',
    };

    const favoritesClickEl = (id) => {
        const isIncludes = props.favorites.includes(id);
        let newFavorite;
        if (!isIncludes) {
            newFavorite = props.favorites.concat(id);
        } else {
            newFavorite = props.favorites.filter(item => item !== id);
        }
        props.setFavoritesAction(newFavorite);
        localStorage.setItem('favorites', JSON.stringify(newFavorite));
    };

    return (
        <div onClick={()=>{favoritesClickEl(props.id)}}>
            {!props.favoritesAdd && <img src={imgUrl}  alt="logo" style={iconStyle}/>}
            {props.favoritesAdd && <img src={imgUrlYellow}  alt="logo" style={iconStyle}/>}
        </div>
    );
}

function mapStateToProps(state) {
    return{
        favorites: state.initInfo.favorites,
    }

}
const  mapDispatchToProps =
    {
        setFavoritesAction,
    };

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);

