import React from 'react';
import BlockCards from "../BlockCards/BlockCards";
import {connect} from "react-redux";
import {setFavoritesAction} from "../../actions/actionProdacts";


function FavoritesBlockCards(props) {

    const shFavorites = props.products.filter((item) =>
        props.favorites.includes(item.id)
    );

    return (
        <div>
            {(shFavorites.length === 0) && 'Избранные товары отсутствуют'}
            <BlockCards
                products={shFavorites}
            />

        </div>
    );
}

function mapStateToProps(state) {
    return{
        products: state.initInfo.products,
        favorites: state.initInfo.favorites
    }
}
const  mapDispatchToProps =
    {
        setFavoritesAction,
    };


export default connect(mapStateToProps, mapDispatchToProps)(FavoritesBlockCards);

