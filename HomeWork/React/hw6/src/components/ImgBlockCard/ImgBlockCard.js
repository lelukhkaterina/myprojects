import React from 'react';
import "./ImgBlockCard.scss"

//Картинка в карточке товара


function ImgBlockCard(props) {
    return (
        <div className={'imgBlockCardStyle'}>
            <img src={props.productsItemsImg} alt="imgProducts"/>
        </div>
    );
}



export default ImgBlockCard;