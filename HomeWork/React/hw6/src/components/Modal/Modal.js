import React from 'react';
import Button from "../Button/Button";
import './StyleModal.scss'

function Modal(props) {
    function handleClick(evt) {
        evt.stopPropagation()
    }

    return (
        <div className={'backgroundStyle'} onClick ={props.closeModalWindow} data-testid={'modalWindow'}>
            <div className={'windowStyle'} onClick ={handleClick} style={{backgroundColor: props.backgroundColor}}>
                <h2>{props.header}</h2>
                <hr/>
                <p>{props.text}</p>
                <div className={'buttonBlock'}>
                    <Button
                        testid={'buttonOk'}
                        text={'Ok'}
                        functionOnClick={props.modalOk}
                        productItemId={props.productItemId}
                        className={"styleButton"}
                    />
                    <Button
                        text={'Close'}
                        functionOnClick={props.closeModalWindow}
                        className={"styleButton"}
                    />
                </div>
            </div>
        </div>
    );
}

export default Modal;