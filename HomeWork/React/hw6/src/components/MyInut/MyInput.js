import React from 'react'
import { useField } from 'formik';
import './MyInput.scss'
function MyInput(props) {
    const { label, name, ...rest } = props;
    const [field, meta] = useField(name);
    //
    // console.log(useField(name))
    return (
        <div>
            <label className={'myInputLabel'}>{label}
                  <input {...field} {...rest} className={'myInputClass'}/>
            </label>
            {meta.touched && meta.error && <div className='error'>{meta.error}</div>}
        </div>

    );
    //     <>
    //         <div>
    //             <label>{label}
    //                 <input {...field} {...rest} />
    //             </label>
    //         </div>
    //         {meta.touched && meta.error && <div className='error'>{meta.error}</div>}
    //     </>
    // )
}

export default MyInput
