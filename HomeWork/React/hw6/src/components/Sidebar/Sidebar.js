import React from 'react';
import './Sidebar.scss'
import ShoppingCart from "../ShoppingCart/ShoppingCart";
import AllFavorite from "../AllFavorite/AllFavorite";
import {Link} from "react-router-dom";

function Sidebar(props) {
    const iconStyle = {
        width: '40px',
        height: '40px',
        padding: '5px',
        cursor: 'pointer',
    };
    return (
        <div className={'sideBar'}>
            <Link to={'/'}>
                <img src={'./imgSite/user.png'}
                     alt="logo"
                     style={iconStyle}
                />
            </Link>
            <Link to={'/favorites'}>
                <AllFavorite
                    iconStyle={iconStyle}
                />
            </Link>
            <Link to={'/cart'}>
                <ShoppingCart
                    iconStyle={iconStyle}
                />
            </Link>
        </div>
    );
}


export default Sidebar;