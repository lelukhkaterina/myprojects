import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Router } from "react-router-dom";
import {createBrowserHistory} from 'history';

import { Provider } from 'react-redux';
// import {createStore, applyMiddleware} from "redux";
import rootReducer from "./reducers";
// import {composeWithDevTools} from "redux-devtools-extension";
// import thunk from "redux-thunk";
import store from "./store/store";



const history = createBrowserHistory();
// const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
          <Router history={history}>
              <App />
          </Router>
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
