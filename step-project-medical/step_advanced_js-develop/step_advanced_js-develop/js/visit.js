let form;
let formObj={};
let testObj={
    id: "2345",
    age: "45",
    bmi: "60",
    descriptionVisit: "Головокружение",
    doctors: "Кардиолог",
    normalPressure: "120",
    pastIllnesses: "Не болел",
    patientName: "Петров Петр Петрович",
    purposeVisit: "Высокое давление",
    urgency: "Приоритетная",
    dateVisit: "2020-08-19",
};

const newVisit = document.querySelector("#newVisit_button");
newVisit.addEventListener("click", ()=>{
    let divFon=document.createElement("div");
    divFon.classList.add("divFon");
    let body = document.querySelector("body");
    body.append(divFon);
    let divModal = document.querySelector(".modal");
    form= new FormVisitCreate("modalEl");
    if(divModal!==null){
        if(confirm("Карточка не сохранена, удалить введенные данные?")){
            divModal.remove();
            form.createForm();
        }
    }
    else{
        form.createForm();
    }

});

const changeButton = document.querySelector("#change_button");
changeButton.addEventListener("click", ()=>{
    let body = document.querySelector("body");
    let divFon=document.createElement("div");
    divFon.classList.add("divFon");
    body.append(divFon);
    // let divModal = document.querySelector(".modal");
    form = new FormChangeCard(testObj, "modalEl");
    form.createForm();
});

const loginBtn = document.querySelector("#login_button");
loginBtn.addEventListener("click", () => {
    let body = document.querySelector("body");
    let divFon=document.createElement("div");
    divFon.classList.add("divFon");
    body.append(divFon);
    // let divModal = document.querySelector(".modal");
    form = new FormLogin("modalEl");
    form.createForm();
});

const logoutBtn = document.querySelector("#logout_button");
logoutBtn.addEventListener("click", () => {
    loginBtn.style.display="block";
    newVisit.style.display="none";
    logoutBtn.style.display="none";
    //удаляется блок с карточками
});

class NewInput {
    constructor(name, textLabel, appendEl, type, min, max) {
        this.name=name;
        this.textLabel = textLabel;
        this.appendEl=appendEl;
        this.type=type;
        this.min=min;
        this.max=max;
    }
    createInput(){
        let appEl=document.querySelector(`.${this.appendEl}`);
        const labelEl = document.createElement("label");
        labelEl.textContent=`${this.textLabel}`;
        let inputEl = document.createElement("input");
        inputEl.name=this.name;
        inputEl.required=true;
        inputEl.type=this.type;
        inputEl.min=this.min;
        inputEl.max=this.max;

        appEl.append(labelEl);
        appEl.append(inputEl);
    }
}
class NewSelect {
    constructor(name, option, textLabel, appendEl, classEl) {
        this.name = name;
        this.textLabel = textLabel;
        this.appendEl=appendEl;
        this.option=option;
        this.classEl=classEl;
    }
    createSelect(){
        console.log(this.appendEl);
        let appEl=document.querySelector(`.${this.appendEl}`);
        const labelEl = document.createElement("label");
        labelEl.textContent=`${this.textLabel}`;
        let selectEl = document.createElement("select");
        selectEl.name=this.name;
        selectEl.classList.add(`${this.classEl}`);
        selectEl.required=true;
        selectEl.insertAdjacentHTML("beforeend","<option value='' disabled selected>Сделайте выбор</option>");
        this.option.forEach( (item)=> {
            selectEl.insertAdjacentHTML("beforeend",`<option>${item}</option>`);
        });
        appEl.append(labelEl);
        appEl.append(selectEl);
    }
}
class NewTextArea {
    constructor(name,textLabel, appendEl) {
        this.name=name;
        this.textLabel = textLabel;
        this.appendEl=appendEl;
    }
    createTextArea(){
        let appEl=document.querySelector(`.${this.appendEl}`);
        const labelEl = document.createElement("label");
        labelEl.textContent=`${this.textLabel}`;
        let textareaEl = document.createElement("textarea");
        textareaEl.name=this.name;
        textareaEl.required=true;
        appEl.append(labelEl);
        appEl.append(textareaEl);
    }
}

class VisitDoctor{
    constructor() {
    }
    createVisit(){

        const urgency=["Обычная", "Приоритетная", "Неотложная"];

        const blockAllEl = document.createElement("fieldset");
        blockAllEl.classList.add("allDoctors");
        let modalForm=document.querySelector(".modalEl");
        console.log(modalForm);
        modalForm.append(blockAllEl);
        new NewInput("dateVisit","Дата визита пациента", "allDoctors", "date").createInput();
        new NewTextArea("purposeVisit","Цель визита", "allDoctors").createTextArea();
        new NewTextArea("descriptionVisit","Краткое описание", "allDoctors").createTextArea();
        new NewSelect("urgency",urgency,"Выберите срочность", "allDoctors", "selectUrgency").createSelect();
        new NewInput("patientName","ФИО", "allDoctors").createInput();



    }


}

class VisitCardiologist extends VisitDoctor {
    constructor() {
        super();
    }
    createVisit(){
        super.createVisit();
        const blockPersonally = document.createElement("fieldset");
        blockPersonally.classList.add("personally");
        let selectUrgency= document.querySelector(".selectUrgency");
        selectUrgency.after(blockPersonally);

        new NewInput("normalPressure","Нормальное давление", "personally", "number", "80").createInput();
        new NewInput("bmi","Индекс массы тела", "personally", "number", "1", "100").createInput();
        new NewInput("pastIllnesses","Перенесенные заболевания", "personally").createInput();
        new NewInput("age","Возраст", "personally","number", "1", "120").createInput();


    }
}
class VisitDentist extends VisitDoctor {
    constructor() {
        super();
    }
    createVisit(){
        super.createVisit();
        const blockPersonally = document.createElement("fieldset");
        blockPersonally.classList.add("personally");
        let selectUrgency= document.querySelector(".selectUrgency");
        selectUrgency.after(blockPersonally);
        new NewInput("dateLastVisit","Дата последнего визита", "personally", "date").createInput();
    }

}
class VisitTherapist extends VisitDoctor {
    constructor() {
        super();
    }
    createVisit(){
        super.createVisit();
        const blockPersonally = document.createElement("fieldset");
        blockPersonally.classList.add("personally");
        let selectUrgency= document.querySelector(".selectUrgency");
        selectUrgency.after(blockPersonally);
        new NewInput("age","Возраст", "personally","number", "1", "120").createInput();
    }
}

class ChangeVisitDoctor {
    constructor(obj) {
    }
}

class Form {
    constructor() {

    }
    createForm (){
        let cardsDesk=document.querySelector(".cards_desk");
        const modalDiv = document.createElement("div");
        const modalForm = document.createElement("form");
        const modalEl = document.createElement("fieldset");
        modalEl.classList.add("modalEl");
        modalForm.prepend(modalEl);

        const buttonSubmit=document.createElement("button");
        buttonSubmit.innerHTML="Submit";
        buttonSubmit.type="submit";
        modalForm.addEventListener("submit", function (event) {
            event.preventDefault();
            form.submitForm();
        });

        const buttonCancel=document.createElement("button");
        buttonCancel.innerHTML="Cancel";
        buttonCancel.type="button";
        buttonCancel.addEventListener("click", ()=>form.cancel());

        modalDiv.classList.add("modal");
        modalForm.classList.add("modal__form");
        modalForm.id="formId";
        buttonSubmit.classList.add("button", "butSub");
        buttonCancel.classList.add("button", "butCan");

        modalDiv.append(modalForm);
        modalForm.append(buttonSubmit,buttonCancel);
        let mainEl=document.querySelector(".main");
        mainEl.append(modalDiv);

    }
    submitForm (){
        formObj={};
        let myForm = document.querySelector("#formId");
        let formEl = myForm.elements;
        let buttonSubmit=document.querySelector("btnSub");
        for (let i=0; i< formEl.length; i++) {
            if (formEl[i].type !== "fieldset" && formEl[i].type !== "button" && formEl[i].type !== "submit"){
                formObj[`${formEl[i].name}`]=formEl[i].value;
            }
        }
        console.log(formObj);
        let divModal = document.querySelector(".modal");
        divModal.remove();
        let divFon = document.querySelector(".divFon");
        divFon.remove();
    }
    cancel(){
        let divModal = document.querySelector(".modal");
        if(confirm("Карточка не сохранена, удалить введенные данные?")){
            divModal.remove();
            let divFon = document.querySelector(".divFon");
            divFon.remove();
        }
    }

}
class FormLogin extends Form {
    constructor(appendEl) {
        super();
        this.appendEl=appendEl;
    }
    createForm(){
        super.createForm();
        const blockAllEl = document.createElement("fieldset");
        blockAllEl.classList.add("allEl");
        let modalForm=document.querySelector(".modalEl");
        console.log(modalForm);
        modalForm.append(blockAllEl);

        new NewInput("login","Введите e-mail", "allEl", "email").createInput();
        new NewInput("password","Введите пароль", "allEl", "password").createInput();

    }
    submitForm (){
        super.submitForm();
        loginBtn.style.display="none";
        newVisit.style.display="block";
        logoutBtn.style.display="block";

    }
    cancel(){
        let divModal = document.querySelector(".modal");
        divModal.remove();
        let divFon = document.querySelector(".divFon");
        divFon.remove();

    }

    validate(){
        //Считать логин пароль, проверить валидность
    }
}
class FormVisitCreate extends Form {
    constructor(appendEl ) {
        super();
        this.appendEl=appendEl;
    }
    createForm(){
        super.createForm();
        const doctors=["Кардиолог", "Стоматолог", "Терапевт"];
        new NewSelect("doctors",doctors,"Выберите врача", this.appendEl, "selectDoctors").createSelect();
        let selectDoctors=document.querySelector(".selectDoctors");
        console.log(selectDoctors);
        selectDoctors.addEventListener("change", function () {
            if (document.querySelector(".allDoctors")!==null) {
                document.querySelector(".allDoctors").remove();
            }
            switch(selectDoctors.selectedIndex){
                case 1: {
                    new VisitCardiologist().createVisit();
                    break;
                }
                case 2:{
                    new VisitDentist().createVisit();
                    break;
                }
                case 3:{
                   new VisitTherapist().createVisit();
                    break;
                }
            }
        });

    }
}


class FormChangeCard extends Form{
    constructor(obj, appendEl) {
        super();
        this.id=obj.id;
        this.doctors=obj.doctors;
        this.appendEl=appendEl;
        this.obj=obj;
    }
    createForm(){

        super.createForm();
        let myForm;
        let formEl;
        function fillInForm(myForm, formEl, obj) {
            for (let i=0; i<formEl.length; i++) {
                if (formEl[i].type !== "fieldset" && formEl[i].type !== "button" && formEl[i].type !== "submit"){
                    let elName=formEl[i].name;
                    formEl[i].value=obj[elName];
                }
            }
        }
        const doctors=["Кардиолог", "Стоматолог", "Терапевт"];
        new NewSelect("doctors",doctors,"Выберите врача", this.appendEl, "selectDoctors").createSelect();
        let selectDoctors=document.querySelector(".selectDoctors");
        switch (this.doctors) {
               case "Кардиолог": {
                   selectDoctors.selectedIndex = 1;
                   new VisitCardiologist().createVisit();
                   myForm = document.querySelector("#formId");
                   formEl = myForm.elements;
                   fillInForm(myForm,formEl,this.obj);
                   break;
               }
              case "Стоматолог":{
                   selectDoctors.selectedIndex=2;
                   new VisitDentist().createVisit();
                   myForm = document.querySelector("#formId");
                   formEl = myForm.elements;
                   fillInForm(myForm,formEl,this.obj);
                   break;
               }
               case "Терапевт":{
                   selectDoctors.selectedIndex=3;
                   new VisitTherapist().createVisit();
                   myForm = document.querySelector("#formId");
                   formEl = myForm.elements;
                   fillInForm(myForm,formEl,this.obj);
                   break;
               }
       }
        selectDoctors.addEventListener("change",  ()=> {
            if (document.querySelector(".allDoctors")!==null) {
                document.querySelector(".allDoctors").remove();
            }
            switch(selectDoctors.selectedIndex){
                case 1: {
                    new VisitCardiologist().createVisit();

                    fillInForm(myForm,formEl,this.obj);
                    selectDoctors.selectedIndex = 1;
                    break;
                }
                case 2:{
                    new VisitDentist().createVisit();

                    fillInForm(myForm,formEl,this.obj);
                    selectDoctors.selectedIndex = 2;
                    break;
                }
                case 3:{
                    new VisitTherapist().createVisit();

                    fillInForm(myForm,formEl,this.obj);
                    selectDoctors.selectedIndex = 3;
                    break;
                }
            }
        });








    }
}

