export default class BdWalker { //Переделал на класс
    constructor() {
        this.API = 'http://cards.danit.com.ua';
        this.LOGIN = '/login';
        this.CARDS = '/cards';
        this.METHODS = {
            post: 'POST',
            get: 'GET',
            put: 'PUT',
            delete: 'DELETE',
        }
    }

    async _getToken(credentials) { // функция получения токена
        const response = await fetch(this.API + this.LOGIN, {
            method: this.METHODS.post,
            body: JSON.stringify({
                email: credentials.email,
                password: credentials.password
            })
        });
        let data = await response.json();
        return data.token;
    }

    async _connect(data) { // Универсальный запрос для альнейшего использования, использует получение токена
        const token = await this._getToken(data.credentials);
        let request = {
            method: data.method,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };
        let link = this.API + this.CARDS;
        if (data.id) { // Если запрос с ID то добавляем
            link = link + '/' + data.id;
        }
        if (data.card) { // Аналогично если есть карточка, то добавляем в запрос
            request.body = JSON.stringify(data.card) // Если запрос типа GET - данного поля не должно быть - ругнется
        }
        const response = await fetch(link, request);
        const content = await response.json()
        console.log(content) // -- > !!!Временно для проверки работоспособности, удалить на релизе!
        return content;
    }

    // ----- Отдельные функции для удаления/изменения/добавления/получения карточек
    async addCard(data) {
        return await this._connect({
            method: this.METHODS.post,
            credentials: data.credentials,
            card: data.card,
        })
    }

    async updateCard(data) {
        return await this._connect({
            method: this.METHODS.put,
            credentials: data.credentials,
            card: data.card,
        })
    }

    async removeCard(data) {
        return await this._connect({
            method: this.METHODS.delete,
            credentials: data.credentials,
            id: data.id,
        })
    }

    async getCard(data) {
        return await this._connect({
            method: this.METHODS.get,
            credentials: data.credentials,
            id: data.id, // если указан ID, то выдаст карточку по данному ID
        })
    }
}
// ------

// Примеры:

const testCard = {
    title: "Визит кererer кардиологу",
    description: 'Планrtrtовый визит',
    doctor: "Cardiorrrlogist",
    bp: "247",
    age: 273,
    weight: 700
}
const walker = new BdWalker();

walker.getCard({
    credentials: {
        'email': "al@ukr.net",
        'password': 12345
    },
    // id: 11392
})

walker.removeCard({
    credentials: {
        'email': "al@ukr.net",
        'password': 12345
    },
    id: 11403,
})

walker.getCard({
    credentials: {
        'email': "al@ukr.net",
        'password': 12345
    },
})

