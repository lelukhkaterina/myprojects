const loginBtn = document.querySelector("#login_button");
const container = document.querySelector(".cards_desk");
console.log(loginBtn);

function craeteModal(selector) {
	const modalDiv = document.createElement("div");
	const modalForm = document.createElement("form");
	const loginField = document.createElement("input");
	const passwordField = document.createElement("input");
	const submitBtn = document.createElement("button");

	loginField.setAttribute("type", "email");
	passwordField.setAttribute("type", "password");

    modalDiv.classList.add("modal");
    modalForm.classList.add("modal__form")
    submitBtn.classList.add("button")

	submitBtn.innerText = "Войти";

	modalForm.append(loginField, passwordField, submitBtn);
	modalDiv.append(modalForm);
	selector.append(modalDiv);
}

loginBtn.addEventListener("click", () => {
	if (!document.querySelector(".modal")) {
		craeteModal(container);
	}
});
